Improved shutdown.sh script that prompts the user to see if they are in an outage scenario

How to add this to your system using puppet:

    file { '/opt/atlassian/jira/bin/shutdown.sh':
        ensure => 'file',
        mode   => 'a+x',
        source => 'https://bitbucket.org/atlasauthority/improved-shutdown/raw/HEAD/shutdown.sh',
    }